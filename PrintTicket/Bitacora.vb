﻿Imports System.IO
Imports System.Configuration

Public Class Bitacora
    Public Sub Log(Mensaje As String)

        Dim Dir As String = Path.Combine(AppDomain.CurrentDomain.BaseDirectory & "Bitacora")
        Dim archivo As String = String.Format("Log_{0}.txt", Now.ToString("ddMMyyyy"))

        If Not Directory.Exists(Dir) Then
            Directory.CreateDirectory(Dir)
        End If

        Dim sw As New StreamWriter(Path.Combine(Dir, archivo), True, Text.Encoding.UTF8)
        sw.WriteLine(String.Format("{0} - {1}", Now.ToString("hh:mm:ss tt"), Mensaje))
        sw.Close()
    End Sub

    Public Sub Log(Mensaje As String, proceso As String, Optional ByRef log As String = "")
        Dim Dir As String = Path.Combine(ConfigurationManager.AppSettings("rutaBitacora").ToString, "Bitacora")
        Dim archivo As String = String.Format("Log{0}_{1}.txt", proceso, Now.ToString("ddMMyyyy"))
        If log <> "" Then
            log = Path.Combine(Dir, archivo)
        End If
        If Not Directory.Exists(Dir) Then
            Directory.CreateDirectory(Dir)
        End If

        Dim sw As New StreamWriter(Path.Combine(Dir, archivo), True, Text.Encoding.UTF8)
        sw.WriteLine(String.Format("{0} - {1}", Now.ToString("hh:mm:ss tt"), Mensaje))
        sw.Close()
    End Sub

    'Public Sub Err(Mensaje As String)
    '    Dim Dir As String = Path.Combine(ConfigurationManager.AppSettings("rutaBitacora").ToString, "Bitacora\Error")
    '    Dim archivo As String = String.Format("Err_{0}.txt", Now.ToString("ddMMyyyy"))

    '    If Not Directory.Exists(Dir) Then
    '        Directory.CreateDirectory(Dir)
    '    End If

    '    Dim sw As New StreamWriter(Path.Combine(Dir, archivo), True, Text.Encoding.UTF8)
    '    sw.WriteLine(String.Format("{0} - {1}", Now.ToString("hh:mm:ss tt"), Mensaje))
    '    sw.Close()
    'End Sub
End Class
