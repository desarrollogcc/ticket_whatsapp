﻿Imports System.Drawing.Printing
Imports System.Drawing
Imports Zen.Barcode
Imports System.Configuration
Imports System.IO.Ports
Imports System.Drawing.Imaging
Imports ThoughtWorks.QRCode
Imports ThoughtWorks.QRCode.Codec
Imports ThoughtWorks.QRCode.Codec.Data

Module Module1
    Private MontoRegalo As String
    Private MontoMinimo As String
    Private Codigo As String
    Private Desde As String
    Private Hasta As String
    Private mes As String
    Private anio As String
    ' cambios para cupon doble
    Private CodigoQR As String
    Private CuponVisible As String

    Sub Main(args() As String)


        '' Prueba()
        Dim log As New Bitacora


        If args.Length <> 0 Then


            Dim pkInstalledPrinters As New List(Of String)
            log.Log("Impresoras instaladas disponibles**********")
            For i = 0 To PrinterSettings.InstalledPrinters.Count - 1
                log.Log("Impresora: " & PrinterSettings.InstalledPrinters.Item(i))
            Next

            Try
                Dim Efeticket As New PrintDocument
                Efeticket.PrinterSettings.PrinterName = ConfigurationManager.AppSettings.Get("NombreImpresora")
                log.Log("Nombre de la impresora Configurada " & ConfigurationManager.AppSettings.Get("NombreImpresora"))
                Efeticket.DefaultPageSettings.Landscape = False
                AddHandler Efeticket.PrintPage, AddressOf Efeticket_PrintPage
                Efeticket.Print()
                log.Log("Imprimiendo ticket")
            Catch ex As Exception
                log.Log("Error: " + ex.Message.ToString)
            Finally
                log.Log("Termina Proceso")
            End Try
        End If
    End Sub

    Private Sub Efeticket_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
        Dim textlocation As Point
        Dim imgeLocation As Point
        Dim newImage As Image
        imgeLocation = New Point(0, 0)


        Dim log As New Bitacora
        log.Log("Inicia diseño del ticket")

        'Inicio header
        imgeLocation = New Point(0, 0)
        newImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory & ConfigurationManager.AppSettings.Get("imagen").ToString)
        e.Graphics.DrawImage(newImage, imgeLocation)
    End Sub


End Module

